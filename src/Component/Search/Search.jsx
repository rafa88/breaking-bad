import React, { useState } from "react";

import "./Search.css";

const Search = ({ login }) => {
  const [userName, setUserName] = useState("");

  const onKeyPress = (e) => {
    if (!e.target.value) {
      login("");
    }
  }
  
  return (
    <form className="form" onSubmit={(e) =>{e.preventDefault(); login(userName);} }>
      <div className="form-group">
        <input
          className="form-input"
          placeholder="search by name"
          onChange={(e) => setUserName(e.target.value)}
          onKeyUp={onKeyPress}
          value={userName}
        />
        <button className="form-button" type="submit"></button>
      </div>
    </form>
  );
};

export default Search;
