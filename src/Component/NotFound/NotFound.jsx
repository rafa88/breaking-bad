import React from "react";

import walter from "../../images/icons8-walter-white.svg";

import "./NotFound.css";

const NotFound = () => (
  <div className="not-found">
    <img src={walter} className="not-found__image" />
    <p>no actor found</p>
  </div>
);

export default NotFound;
