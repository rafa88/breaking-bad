import React from "react";
import "./Actor.css";
import Highlight from "../Highlight/Highlight";

function Actor({ actor, searchText }) {
  return (
    <div className="actor">
      <div className="actor-photo">
        <img src={actor.img} alt={actor.name} />
      </div>
      <div className="actor-detail">
        <p className="actor-detail__name">
          <Highlight word={actor.name} searchText={searchText} />
          <br />
          {actor.nickname}
        </p>
        <ul className="actor-detail__description">
          <li className="actor-detail__description-item">
            <div>
              <strong>Occupation:</strong>{" "}
              <span>{actor.occupation.join()}</span>
            </div>
          </li>
          <li className="actor-detail__description-item">
            <div>
              <strong>Seasons:</strong>{" "}
              <span>{actor.appearance && actor.appearance.join()}</span>
            </div>
          </li>
          <li className="actor-detail__description-item">
            <div>
              <strong>Status:</strong> <span>{actor.status}</span>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Actor;
