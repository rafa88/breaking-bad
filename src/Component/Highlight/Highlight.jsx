import React, { Fragment } from "react";
import "./Highlight.css";

const Highlight = ({ word, searchText }) => {
  const searchParams = searchText ? searchText.toLowerCase() : searchText;
  const index = word.toLowerCase().indexOf(searchParams);
  if (index >= 0) {
    return (
      <Fragment>
        {word.substring(0, index)}
        <span className="highlight">
          {word.substring(index, index + searchText.length)}
        </span>
        {word.substring(index + searchText.length)}
      </Fragment>
    );
  } else {
    return word;
  }
};

export default Highlight;
