export async function getActor() {
  return fetch("https://www.breakingbadapi.com/api/characters", {
    limit: 10,
    offset: 10,
  })
  .then((response) => response.json())
}
