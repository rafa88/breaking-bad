import React, { useState, useEffect } from "react";

import NotFound from "./Component/NotFound/NotFound";
import Loading from "./Component/Loading/Loading";
import Search from "./Component/Search/Search";
import Actor from "./Component/Actor/Actor";
import { getActor } from "./Request/Actor";

import "./App.css";

const App = () => {
  const [actors, setActors] = useState([]);
  const [loading, setLoading] = useState(true);
  const [filteredActor, setFilteredActor] = useState(null);

  useEffect(() => {
    getActor()
      .then((data) => {
        setActors(data);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  }, []);

  const req = filteredActor ? new RegExp(filteredActor, "i") : null;
  const filteredActors = filteredActor
    ? actors.filter((actor) => {
        const match = actor.name.match(req);
        if (!match) {
          return null;
        } else {
          return { ...actor };
        }
      })
    : actors;

  return (
    <div className="App">
      {loading && <Loading />}
      <header className="header">
        <h1>The Breaking Bad</h1>
      </header>
      <Search login={(name) => setFilteredActor(name)} />

      {!loading && (
        <div className="actors">
          {filteredActors.map((actor) => (
            <Actor
              key={actor.char_id}
              actor={actor}
              searchText={filteredActor}
            />
          ))}
        </div>
      )}
      {!loading && filteredActors.length === 0 && (
        <NotFound />
      )}
    </div>
  );
};

export default App;
